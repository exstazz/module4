using System;
using System.Linq;

namespace M4
{
    public class Module4
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Module4");
        }

        public int Task_1_A(int[] array)
        {
            int res = 0;
            if (array == null || array.Length == 0) throw new ArgumentNullException(nameof(array));
            else
            {
                res = array.Max();
            }
            Console.WriteLine(res);
            return res;
        }

        public int Task_1_B(int[] array)
        {
            int res = 0;
            if (array == null || array.Length == 0) throw new ArgumentNullException(nameof(array));
            else
            {
                res = array.Min();
            }
            Console.WriteLine(res);
            return res;
        }

        public int Task_1_C(int[] array)
        {
            int res = 0;
            if (array == null || array.Length == 0) throw new ArgumentNullException(nameof(array));
            else
            {
                res = array[0];

                for (int i = 1; i < array.Length; i++)
                {
                    res = res + array[i];
                }
            }
            Console.WriteLine(res);
            return res;
        }

        public int Task_1_D(int[] array)
        {
            int res = 0;
            if (array == null || array.Length == 0) throw new ArgumentNullException(nameof(array));
            else
            {
                int max = array.Max();
                int min = array.Min();
                res = max - min;
            }
            Console.WriteLine(res);

            return res;
        }

        public void Task_1_E(int[] array)
        {
            if (array == null || array.Length == 0) throw new ArgumentNullException(nameof(array));
            else
            {
                int max = array.Max();
                int min = array.Min();
                for (int i = 0; i < array.Length; i++)
                {
                    if (i % 2 == 0) array[i] += max;
                    else array[i] -= min;
                }


                for (int i = 0; i < array.Length; i++)
                {
                    Console.WriteLine(array[i]);
                }
            }
        }

        public int Task_2(int a, int b, int c)
        {
            int res = a + b + c;
            Console.WriteLine(res);
            return res;
        }

        public int Task_2(int a, int b)
        {
            int res = a + b;
            Console.WriteLine(res);
            return res;
        }

        public double Task_2(double a, double b, double c)
        {
            double res = a + b + c;
            Console.WriteLine(res);
            return res;
        }

        public string Task_2(string a, string b)
        {
            string res = a + b;
            Console.WriteLine(res);
            return res;
        }

        public int[] Task_2(int[] a, int[] b)
        {
            if (a == null || a.Length == 0) throw new ArgumentNullException(nameof(a));
            if (b == null || b.Length == 0) throw new ArgumentNullException(nameof(b));
            if (a.Length > b.Length)
            {
                for (int i = 0; i < b.Length; i++)
                {
                    a[i] += b[i];
                }
                for (int i = 0; i < a.Length; i++)
                {
                    Console.WriteLine(a[i]);
                }
                return a;
            }
            else
            {
                for (int i = 0; i < a.Length; i++)
                {
                    b[i] += a[i];
                }
                for (int i = 0; i < b.Length; i++)
                {
                    Console.WriteLine(b[i]);
                }
                return b;
            }

        }

        public void Task_3_A(ref int a, ref int b, ref int c)
        {
            a += 10;
            b += 10;
            c += 10;
            Console.WriteLine(a.ToString());
            Console.WriteLine(b.ToString());
            Console.WriteLine(c.ToString());
        }

        public void Task_3_B(double radius, out double length, out double square)
        {
            if (radius < 0) throw new ArgumentNullException(nameof(radius));
            length = 2 * Math.PI * radius;
            square = Math.PI * Math.Pow(radius, 2);
            Console.WriteLine(radius);
            Console.WriteLine(length);
        }

        public void Task_3_C(int[] array, out int maxItem, out int minItem, out int sumOfItems)
        {
            int res = 0;
            int min = 0;
            int max = 0;
            if (array == null || array.Length == 0) throw new ArgumentNullException(nameof(array));
            else
            {
                min = int.MaxValue;
                for (int i = 0; i < array.Length; i++)
                {
                    if (array[i] < min)
                    {
                        min = array[i];
                    }
                }

                max = int.MinValue;
                for (int i = 0; i < array.Length; i++)
                {
                    if (array[i] > max)
                    {
                        max = array[i];
                    }
                }
            }
            minItem = min;
            maxItem = max;
            for (int i = 0; i < array.Length; i++)
            {
                res = res + array[i];
            }
            sumOfItems = res;
            Console.WriteLine(minItem);
            Console.WriteLine(maxItem);
            Console.WriteLine(sumOfItems);
        }

        public (int, int, int) Task_4_A((int, int, int) numbers)
        {
            numbers.Item1 += 10;
            numbers.Item2 += 10;
            numbers.Item3 += 10;
            Console.WriteLine(numbers.Item1);
            Console.WriteLine(numbers.Item2);
            Console.WriteLine(numbers.Item3);
            return numbers;
        }

        public (double, double) Task_4_B(double radius)
        {
            double length;
            double square;
            if (radius < 0)
            {
                throw new ArgumentNullException(nameof(radius));
            }
            length = 2 * Math.PI * radius;
            square = Math.PI * Math.Pow(radius, 2);
            Console.WriteLine(radius);
            Console.WriteLine(length);
            return (length, square);
        }

        public (int, int, int) Task_4_C(int[] array)
        {
            int maxItem;
            int minItem;
            int sumOfItems;
            int res = 0;
            int min = 0;
            int max = 0;
            if (array == null || array.Length == 0) throw new ArgumentNullException(nameof(array));
            else
            {
                min = int.MaxValue;
                for (int i = 0; i < array.Length; i++)
                {
                    if (array[i] < min)
                    {
                        min = array[i];
                    }
                }

                max = int.MinValue;
                for (int i = 0; i < array.Length; i++)
                {
                    if (array[i] > max)
                    {
                        max = array[i];
                    }
                }
            }
            minItem = min;
            maxItem = max;
            for (int i = 0; i < array.Length; i++)
            {
                res = res + array[i];
            }
            sumOfItems = res;
            Console.WriteLine(minItem);
            Console.WriteLine(maxItem);
            Console.WriteLine(sumOfItems);
            return (minItem, maxItem, sumOfItems);
        }

        public void Task_5(int[] array)
        {
            if (array == null || array.Length == 0) throw new ArgumentNullException(nameof(array));
            else
            {
                for (int i = 0; i < array.Length; i++)
                {
                    array[i] += 5;
                    Console.WriteLine(array[i]);
                }
            }
        }

        public void Task_6(int[] array, SortDirection direction)
        {
            if (array == null || array.Length == 0) throw new ArgumentNullException(nameof(array));
            else
            {
                if (direction == SortDirection.Ascending)
                {
                    Array.Sort(array);
                    for (int i = 0; i < array.Length; i++)
                    {
                        Console.WriteLine(array[i]);
                    }
                }
                else
                {

                    Array.Sort(array);
                    Array.Reverse(array);
                    for (int i = 0; i < array.Length; i++)
                    {
                        Console.WriteLine(array[i]);
                    }
                }
            }
        }

        public double Task_7(Func<double, double> func, double x1, double x2, double e, double result = 0)
        {
            if (func == null) throw new ArgumentNullException(nameof(func));
            else
            {
                double c = (x2 + x1) / 2.0f;

                if (func(c) == 0 || x1 + x2 < e)
                    return c;

                if (func(c) * func(x1) < 0)
                    return Task_7(func, x1, c, e, result);
                else
                    return Task_7(func, c, x2, e, result);
            }
        }
    }
}